<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Quiz Application</title>

</head>
  <body>
<center>
<div class="card border-dark mb-3" style="height: 30rem; width:30rem; background-color:powderblue; border:3px solid; text-align: left;">
  <div class="card-body">
    <h1 class="card-title"><b>Instruction</b></h1><br>
    <ol>
    <li>This app is for 2020-21 online Multiple choice question examination as per time table pulished by SRWC made by student.</li><br>
    <li>This online examination contain 5 MCQ question all question are compalsory to solve.</li><br>
    <li>There is no negtaive marking.</li><br>
    <li>Login use the username password allotted to you which is printed on exam card.</li><br>
    </ol>
    <a href="index.php" class="btn btn-dark" style='margin-left:70px'>Back</a>
  </div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</center>
  </body>
</html>
