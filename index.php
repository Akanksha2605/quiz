<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	
    <title>Quiz Application</title>

</head>
  <body>
<center>
<div class="card text-center card border-dark mb-3" style="height: 30rem; width:30rem; background-color:powderblue; border:3px solid;">
  <div class="card-body">
    <h1 class="card-title">Welcome to SRWC</h1><br><br>
    <img src="srwc.jpg"></img><br><br><br><br>
    <a href="inst.php" class="btn btn-dark" style='margin-right:70px'> Instruction</a>
    <a href="login_form.php" class="btn btn-dark">Login</a>
    <a href="help.php" class="btn btn-dark" style='margin-left:70px'>Help</a>	
  </div>
</div>	
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</center>
  </body>
</html>
