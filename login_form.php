<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Login</title>
	<style>
#login .container #login-row #login-column #login-box {
  max-width: 600px;
  height: 300px;
  border: 1px solid #9C9C9C;
  background-color: #EAEAEA;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 10px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: -75px;
}
</style>
</head>
  <body>
<center>
<div class="card text-center card border-dark mb-3" style="height: 30rem; width:30rem; background-color:powderblue; border:3px solid;">
  <div class="card-body">
    <h1 class="card-title">Login Form</h1><br><br>
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-10">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="login_action.php" method="post">
                            <div class="form-group">
                                <label for="username" class="text-dark"><b>Username:</b></label><br>
                                <input type="text" name="username" id="name" class="form-control">
                            </div><br>
                            <div class="form-group">
                                <label for="password" class="text-dark"><b>Password:</b></label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div><br>
                            <div class="form-group">
                                <label for="remember-me" class="text-dark"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br><br>
                                <input type="submit" name="submit" class="btn btn-dark btn-md" style='margin-right:70px' value="Login">
				 <a href="index.php" class="btn btn-dark" style='margin-left:70px'>Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</center>
  </body>
</html>
